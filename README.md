# Law Office of Katherine OBrien

Law firm located in Gloucester County, New Jersey, providing experienced and affordable legal representation for New Jersey expungements and name changes, TSA PreCheck and Global Entry appeals, and TWIC and HAZMAT appeals and waiver.

Address: 288 Egg Harbor Road, Suite 9-105, Sewell, NJ 08080, USA

Phone: 856-832-2482
